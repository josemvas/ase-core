This is an alternative packaging of the Atomic Simulation Environment (ASE)
that only depends on numpy in order to create smaller Binder images.

The most basic functionality only requires numpy but if you need more
functionality you must manually install scipy>=1.4.1 or matplotlib>=3.3.4. as required.

The module names are not changed so this package can be used as a drop-in
replacement of regular ASE.
